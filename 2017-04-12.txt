So what I want to do is not something I am fully capable of expressing.
Somehow, I think this ^ itself is the key.

What is the question?
Is there a manner of programming that allows exploration of the system without defining the system itself?
Or does that question rely on having already had all manners of computation defined?
Can you build a system that takes in expressions that the machine itself cannot define, and through that system come to an alternate form that itself is entirely defined, and yet also fits perfectly the original expression?

Another issue to ponder:

x: (+ 2 2 (- 2))
y: (+ 2)

x adds two twice after it subtracts two.
y adds two.

Mathmatically these evaluate the same, but not from a computational standpoint.
A decent compiler might transpile x into y as there is no need for the additional steps. So from a computational standpoint, they could be the same function with one written poorly, depending on how the system evaluates.
But what if x relies on that subtraction for some reason? What if the only reason we wrote this was to have the system access the symbol, 2, three times within its evaluation? The functions have different side effects, one being the eval count and time, the other being the compiler dropping two evaluations. 

What I am coming to the conclusion of is: any system that can programatically do exactly what I want it to do, never understands exactly what I want without a great amount of struggle on my part to exactly define myself.

Somehow I think this is the wrong way to go about it. Not exactly. What I mean is, perhaps the system doesn't transpile those functions without confirmation that the extra bits aren't "bound" into another system part? What if the compiler was able to find a reason to choose one of the two, and in fact could choose anything that fits the results as they result? Can this be done?

What if I have been looking at computational systems all wrong? What if functions are a solved problem, and are just waiting to be catalogged?

What if the real issue is explaining exactly what we mean to the computers? Can we build a system that takes in badly formed arguements and reforms them for clarity while not deforming them for the user? What if the system found a dozen functions that fit any given need, stored the results, and whittled the specificity down when a bad-result gets calculated? How would it ever know? What if a system was designed to appear extremely adversarial? Or so as to have all badly formed expressions fully explored over time? What if we don't really need our computation to be mathmatically exact? What if the computation itself is more exacting than our math? What if the computer was free to do either x or y when asked to add 2 to something and chooses itself?

Is that not what functional programming sets out to do? Allow the system to make choices based on required expressional outcomes.

z: (add-by-results 2)

So then the question is, what would add-by-results look like?

So many questions today.

What if there was a system that did not require + 2 in the first place? Or is this just virtualization?

What if the functionality was acheived through:

a: (bind
    result: + 2
    effect: access two, bound three times
    effect: access plus AND minus
    )
Now we have a function that expresses what we want to do, right up till the last line. This doesn't require a two to be added or subtracted beyond the initial addition that the results required. Access would be a poor fitting word for the function, as would result and effect. What we need is a higher level conceptualization.

This is fixed easily enough by dropping ambiguous words.

b: (bind + 2
    bound: + 2
    bound: - 2
    bound: 2 2 2
    )

This looks better. The first line calls for the results, the rest of the lines provide additional requirements.
But does the system know that the first bounds are the same twos the third bounds adds? Who cares?

Perhaps this is more clear:

c: (bind + 2
    bound: - 2
    bound: + 2
    )    
    
Now we are talking specificity.
This looks for resulting functions that result in the addition of two, while implicitly also adding and subtracting two along the way.

Note that this is assuming all bound symbols to be explicitly the only ones bound. If I wanted to specify unbounds, say to ensure a three never is bound up, we would need some sort of signal for that.

And perhaps result was worth having as a return line?

d: (bind + - 2 2 2
    unbound: 3
    return bound(+ 2)
    )

How many functions could fit this shape, generated randomly in a minute? How many functions could fit this shape, given infinite time and processing power? Wouldn't the upper bound be time-limited and never complete?

Perhaps we need time to be bound, as a means of checking?

e: (bind + - 2 2 2 time-limit(30s)
    return bound(+ 2)
    )
    
I don't like how this format is going. Lets re-write this to be more clear. Back to dropping the return line, I don't think we need it.

f: (bind + 2
    bound: + - 2 2 2 time-limit(30s)
    )
    
Lets reverse the concept of a time limit to include an unbound:
g: (bind + 2
    bound: + - 2 2 2
    unbound: time-over-limit(30s)
    )

Now we have something interesting. 
Truth,  falseness, and the borderline that separates them, would be able to be defined through this system of bindings:

true: (bind
    unbound: null
    )
    
false: (bind
    bound: null
    )

fine-line: (bind
    bound: truth
    unbound: false
    )

This defines truth as anything that exists in the computer from a switches-existing standpoint, while defining false as anything the computer is unable to do. Thus the fine line becomes clear that both truth and falseness must be understood in order to balance on that line.

Does this accurately depict truth, as currently in use by either humans or computers?

Clearly any computation could be described as such. However, the number of unbounds and bindings within one would obviously grow to include all others in the search for the set of all functions - this is why I wanted an unbound key, to define where tests fail.

Does such a system exist? Yes. Lisp + ContextL can provide a graph-based framework for producing such a system.
From there I can re-code contextL itself.
The end result would be a self-producable system.

But this does not solve my issue. We are closer, though.
What we have been given by contextL is a way to express something though a series of tests.
Is a series of tests enough for a human to, without high cognitive load, express exactly what they want, without ever directly expressing it? It certainly is the orthogonal manner in which I am more comfortable. But this is only part way there.
What is missing is the looseness of understanding humans excel at operating inside of.
What if there was no core? What if the system can be defined through a mechanical process as a function of relays/switches/transistors? Anything that can be done in transistors can be done in math and therefore as a function simulated inside a virtual system.
What if the mechanical process isn't static and thus cannot be well defined?
What if this needs to grow and shrink through those switching processes?

How would a system that has no ill formed types be defined? Let the system be a group of functions that find other functions within operational paradigms. Let that system redefine itself through time with time as another dimension.

There is a reason I am looking to describe things by their bindings and not entirely directly. This is to avoid repeating functionality. Effectively a search through the bindings for n time bound functions to acheive the requirements, followed by testing to see if similar requirements have better performance? Clearly every function would need to be bound to the time it takes to run. I, being a human, have no time for any of that. So I am working on making my computers do this for me.

Eventually I will compute everything that could have been in the time I get and the number of machines, minus all the things computed beforehand. In english, even with a nack for making complicated things sound somewhat simple, this is a terriblely complicated idea to understand. Hmmm...

test-new-function (bind
    bound:old-functions-sorted
    unbound:test-for-worse-than-log-n
    )

Now, how to sort and how to test...

Is this turing complete, on hardware? I have a specific goal here.













